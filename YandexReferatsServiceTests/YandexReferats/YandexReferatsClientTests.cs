﻿using NUnit.Framework;
using YandexReferatsService.Domain;
using YandexReferatsService.YandexReferats;

namespace YandexReferatsServiceTests.YandexReferats
{
    [TestFixture]
    public class YandexReferatsClientTests
    {
        [Test]
        public void GetReferatTextAsync_ForAGivenTopic_ShouldReturnTextWithoutHtml()
        {
            var sut = new YandexReferatsClient();

            var text = sut.GetReferatText(new[] {ReferatTopics.Mathematics});

            Assert.That(text, Does.Contain("Реферат по математике"));
            Assert.That(text, Does.Not.Contain("математикеТема"));
            Assert.That(text, Does.Not.Contain("</div>"));
        }
    }
}