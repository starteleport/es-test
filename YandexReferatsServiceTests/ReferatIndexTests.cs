﻿using System;
using System.Linq;
using System.Threading.Tasks;
using NUnit.Framework;
using YandexReferatsService;
using YandexReferatsService.Domain;

namespace YandexReferatsServiceTests
{
    [TestFixture]
    public class ReferatIndexTests
    {
        private ReferatIndex sut;

        [OneTimeSetUp]
        public void SetUp()
        {
            sut = new ReferatIndex();
            sut.Initialize();
        }

        [Test]
        public async Task Query_CountsByTopic_ShouldRespectQueryOnly()
        {
            var soughtTerm = Guid.NewGuid().ToString("N");
            var otherTerm = Guid.NewGuid().ToString("N");

            var chocoPsycho = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Psychology}};
            var chocoLaw = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Law}};
            var pandaLaw = new ReferatDocument {Text = otherTerm, Topics = new[] {ReferatTopics.Law}};

            sut.IndexMany(new[] {chocoPsycho, chocoLaw, pandaLaw});
            sut.Refresh();

            var actual = await sut.Query(soughtTerm, new[] {ReferatTopics.Psychology});

            Assert.That(actual.CountsByTopic[ReferatTopics.Law], Is.EqualTo(1));
            Assert.That(actual.CountsByTopic[ReferatTopics.Psychology], Is.EqualTo(1));
        }

        [Test]
        public async Task Query_TextWithTopic_ShouldFilterCorrectly()
        {
            var soughtTerm = Guid.NewGuid().ToString("N");
            var otherTerm = Guid.NewGuid().ToString("N");

            var chocoPsycho = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Psychology}};
            var chocoLaw = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Law}};
            var pandaLaw = new ReferatDocument {Text = otherTerm, Topics = new[] {ReferatTopics.Law}};

            sut.IndexMany(new[] {chocoPsycho, chocoLaw, pandaLaw});
            sut.Refresh();

            var actual = await sut.Query(soughtTerm, new[] {ReferatTopics.Psychology});

            Assert.That(actual.Hits, Has.Length.EqualTo(1));
            Assert.That(actual.Hits.Single().Topics, Is.EquivalentTo(new[] {ReferatTopics.Psychology}));
            Assert.That(actual.Hits.Single().Text, Is.EqualTo(soughtTerm));
        }

        [Test]
        public async Task Query_TextWithoutTopic_ShouldFilterCorrectly()
        {
            var soughtTerm = Guid.NewGuid().ToString("N");
            var otherTerm = Guid.NewGuid().ToString("N");

            var chocoPsycho = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Psychology}};
            var chocoLaw = new ReferatDocument {Text = soughtTerm, Topics = new[] {ReferatTopics.Law}};
            var pandaLaw = new ReferatDocument {Text = otherTerm, Topics = new[] {ReferatTopics.Law}};

            sut.IndexMany(new[] {chocoPsycho, chocoLaw, pandaLaw});
            sut.Refresh();

            var actual = await sut.Query(soughtTerm, null);

            Assert.That(actual.Hits, Has.Length.EqualTo(2));
            Assert.That(actual.Hits,
                        Has.Some.Matches(
                               (ReferatDocument d) => d.Topics.Single() == ReferatTopics.Law));

            Assert.That(actual.Hits,
                        Has.Some.Matches(
                               (ReferatDocument d) => d.Topics.Single() == ReferatTopics.Psychology));
            Assert.That(actual.Hits,
                        Has.All.Matches(
                               (ReferatDocument d) => d.Text == soughtTerm));
        }
    }
}