﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace YandexReferatsService.Domain
{
    [JsonConverter(typeof(StringEnumConverter))]
    public enum ReferatTopics
    {
        Astronomy,
        Geology,
        Gyroscope,
        Literature,
        Marketing,
        Mathematics,
        Music,
        Polit,
        Agrobiologia,
        Law,
        Psychology,
        Geography,
        Physics,
        Philosophy,
        Chemistry,
        Estetica
    }
}