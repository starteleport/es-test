﻿namespace YandexReferatsService.Domain
{
    public class ReferatDocument
    {
        public ReferatTopics[] Topics { get; set; }
        
        public string Text { get; set; }
    }
}
