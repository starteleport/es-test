﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Nest;
using YandexReferatsService.Domain;

namespace YandexReferatsService
{
    public class ReferatIndex
    {
        private const string ReferatsIndex = "referats";
        private static readonly int TopicsCount = Enum.GetValues(typeof(ReferatTopics)).Length;
        private readonly IElasticClient elasticClient;

        public ReferatIndex()
        {
            var elasticUri = new Uri(ConfigurationManager.ConnectionStrings["es"].ConnectionString);

            var elasticSettings = new ConnectionSettings(elasticUri);
            elasticSettings.DefaultIndex(ReferatsIndex);

            elasticClient = new ElasticClient(elasticSettings);
        }

        public void Initialize()
        {
            var index = new CreateIndexDescriptor(ReferatsIndex);
            index.Mappings(
                m =>
                    m.Map<ReferatDocument>(
                        r =>
                            r.Properties(
                                 pd =>
                                     pd.Keyword(k => k.Name(p => p.Topics))
                                       .Text(s => s.Name(p => p.Text).Analyzer("russian").SearchAnalyzer("russian")))
                             .AutoMap()));

            elasticClient.CreateIndexAsync(index);
        }

        public void IndexMany(IEnumerable<ReferatDocument> referats)
        {
            elasticClient.IndexMany(referats);
        }

        public void Refresh()
        {
            elasticClient.Refresh(new RefreshRequest(ReferatsIndex));
        }

        public async Task<ReferatSearchResults> Query(string text, ReferatTopics[] topics)
        {
            var search = new SearchDescriptor<ReferatDocument>()
                .Source()
                .Aggregations(a => a.Terms("count_by_topic", t => t.Field(f => f.Topics).Size(TopicsCount)));

            search = ApplyQueryText(text, search);
            search = ApplyTopicFilter(topics, search);

            var searchResult = await elasticClient.SearchAsync<ReferatDocument>(search, CancellationToken.None);

            return MapToResults(searchResult);
        }

        private static SearchDescriptor<ReferatDocument> ApplyTopicFilter(ReferatTopics[] topics,
                                                                          SearchDescriptor<ReferatDocument> search)
        {
            if (topics != null && topics.Any())
            {
                Func<QueryContainerDescriptor<ReferatDocument>, QueryContainer> topicsFilter =
                    sb => sb.Terms(t => t.Field(f => f.Topics).Terms(topics));

                return search.PostFilter(topicsFilter);
            }

            return search;
        }

        private static SearchDescriptor<ReferatDocument> ApplyQueryText(string text, SearchDescriptor<ReferatDocument> search)
        {
            if (!string.IsNullOrWhiteSpace(text))
            {
                Func<QueryContainerDescriptor<ReferatDocument>, QueryContainer> textMatch =
                    mb => mb.Match(r => r.Field(f => f.Text).Query(text));

                return search.Query(q => q.Bool(b => b.Must(textMatch)));
            }

            return search;
        }

        private ReferatSearchResults MapToResults(ISearchResponse<ReferatDocument> searchResult)
        {
            return new ReferatSearchResults
                   {
                       Hits = searchResult.Documents.ToArray(),
                       CountsByTopic = MapAggregationResults(searchResult)
                   };
        }

        private static Dictionary<ReferatTopics, long> MapAggregationResults(
            ISearchResponse<ReferatDocument> searchResult)
        {
            return ((BucketAggregate) searchResult.Aggregations["count_by_topic"])
                .Items
                .Cast<KeyedBucket<object>>()
                .ToDictionary(
                    b => (ReferatTopics) Enum.Parse(typeof(ReferatTopics), b.Key.ToString(), true),
                    b => b.DocCount.Value);
        }
    }
}
