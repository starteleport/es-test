﻿using System.Collections.Generic;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using YandexReferatsService.Domain;

namespace YandexReferatsService.YandexReferats
{
    public class YandexReferatsClient
    {
        private static readonly string YandexReferatsEndpoint = "https://yandex.ru/referats/write/";
        private static readonly Regex TagStripper = new Regex(@"\</?\w+.*?\>", RegexOptions.Compiled);

        public string GetReferatText(IEnumerable<ReferatTopics> topics)
        {
            using (var client = new WebClient())
            {
                client.BaseAddress = YandexReferatsEndpoint;
                client.Encoding = Encoding.UTF8;
                
                var query = "?t=" + string.Join("+", topics).ToLower();
                var html = client.DownloadString(query);
                return FilterTags(html);
            }
        }

        private string FilterTags(string html)
        {
            return TagStripper.Replace(html, " ");
        }
    }
}
