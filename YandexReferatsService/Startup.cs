﻿using System.Web.Http;
using Owin;

namespace YandexReferatsService
{
    public class Startup
    {
        public void Configuration(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.MapHttpAttributeRoutes();
            
            appBuilder.UseWebApi(config);
        }
    }
}
