﻿using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using YandexReferatsService.Domain;

namespace YandexReferatsService
{
    [RoutePrefix("search")]
    public class SearchController : ApiController
    {
        private readonly ReferatIndex referatIndex;

        public SearchController()
        {
            referatIndex = new ReferatIndex();
        }

        [Route("")]
        [HttpGet]
        public async Task<JsonResult<ReferatSearchResults>> Search(string query = null,
                                                                   [FromUri] ReferatTopics[] topics = null)
        {
            return Json(await referatIndex.Query(query, topics));
        }
    }
}
