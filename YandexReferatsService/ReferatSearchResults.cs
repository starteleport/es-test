﻿using System.Collections.Generic;
using YandexReferatsService.Domain;

namespace YandexReferatsService
{
    public class ReferatSearchResults
    {
        public ReferatDocument[] Hits { get; set; }
        public Dictionary<ReferatTopics, long> CountsByTopic { get; set; }
    }
}