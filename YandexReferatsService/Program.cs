﻿using System;
using Microsoft.Owin.Hosting;
using YandexReferatsService.YandexReferats;

namespace YandexReferatsService
{
    class Program
    {
        static void Main(string[] args)
        {
            const string httpListenerPrefix = "http://+:1001";
            const int documentsCount = 50;

            var bootstrapper = GetBootstrapper();
            bootstrapper.Bootstrap(documentsCount);

            using (WebApp.Start<Startup>(httpListenerPrefix))
            {
                Console.WriteLine("Listening on {0}", httpListenerPrefix);
                Console.ReadLine();
            }
        }

        private static Bootstrapper GetBootstrapper()
        {
            var referatsIndex = new ReferatIndex();
            referatsIndex.Initialize();

            return new Bootstrapper(referatsIndex, new YandexReferatsClient());
        }
    }
}
