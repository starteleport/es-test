﻿using System;
using System.Collections.Generic;
using System.Linq;
using YandexReferatsService.Domain;
using YandexReferatsService.YandexReferats;

namespace YandexReferatsService
{
    public class Bootstrapper
    {
        private readonly ReferatIndex referatsIndex;
        private readonly YandexReferatsClient yandexReferatsClient;

        public Bootstrapper(ReferatIndex referatsIndex, YandexReferatsClient yandexReferatsClient)
        {
            this.referatsIndex = referatsIndex;
            this.yandexReferatsClient = yandexReferatsClient;
        }

        public void Bootstrap(int documentsCount)
        {
            var topicCountRandom = new Random();

            var referats = Enumerable.Range(1, documentsCount)
                                     .Select(_ => RandomTopics().Distinct().Take(topicCountRandom.Next(1, 5)).ToArray())
                                     .Select(ts => new ReferatDocument
                                                   {
                                                       Text = yandexReferatsClient.GetReferatText(ts),
                                                       Topics = ts.ToArray()
                                                   })
                                     .ToArray();

            referatsIndex.IndexMany(referats);
        }

        private IEnumerable<ReferatTopics> RandomTopics()
        {
            var topics = (ReferatTopics[]) Enum.GetValues(typeof(ReferatTopics));

            var random = new Random();
            while (true)
            {
                yield return topics[random.Next(topics.Length)];
            }
        }
            
    }
}
